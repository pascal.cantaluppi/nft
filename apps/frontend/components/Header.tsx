import React from 'react';
import Head from 'next/head';

class Header extends React.Component {
  render() {
    return (
      <Head>
        <title>unterwater.NFT</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="description" content="Underwater NFT collections" />
        <meta
          name="keywords"
          content="underwater photographer nft scuba diving"
        />
      </Head>
    );
  }
}

export default Header;
