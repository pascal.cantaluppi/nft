import React from 'react';

class TeamMember extends React.Component {
  render() {
    return (
      <>
        {' '}
        <div className="col-xl-3 col-lg-4 col-md-6">
          <div className="member" data-aos="fade-up">
            <div className="pic">
              <img src="/img/team/team-1.jpg" alt="" />
            </div>
            <h4>Pascal Cantaluppi</h4>
            <span>Photographer</span>
            <div className="social">
              <a href="">
                <i className="bi bi-twitter"></i>
              </a>
              <a href="">
                <i className="bi bi-facebook"></i>
              </a>
              <a href="">
                <i className="bi bi-instagram"></i>
              </a>
              <a href="">
                <i className="bi bi-linkedin"></i>
              </a>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default TeamMember;
