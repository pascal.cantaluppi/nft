import React from 'react';
import TeamMember from './TeamMember';

class Team extends React.Component {
  render() {
    return (
      <>
        <section id="team" className="team section-bg">
          <div className="container">
            <div className="section-title">
              <h2 data-aos="fade-in">Team</h2>
              <p data-aos="fade-in">
                Magnam dolores commodi suscipit. Necessitatibus eius consequatur
                ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
                quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
                Quia fugiat sit in iste officiis commodi quidem hic quas.
              </p>
            </div>
            <div className="row">
              <TeamMember />
              <TeamMember />
              <TeamMember />
              <TeamMember />
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default Team;
