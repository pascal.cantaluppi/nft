import * as React from 'react';
import { Image } from 'react-bootstrap';
import Header from '../components/Header';
import BxlFacebookCircleIcon from '../components/Icons/BxlFacebookCircleIcon';
import Team from '../components/Team';
//import styles from './index.module.css';

export function Index() {
  // <div className={styles.page}>

  function year() {
    const current = new Date();
    return <>{current.getFullYear()}</>;
  }

  return (
    <>
      <Header />
      <header id="header">
        <div className="container d-flex align-items-center justify-content-between">
          <div className="logo">
            <h1>
              <a href="index.html">
                <span style={{ textTransform: 'lowercase' }}>underwater</span>
                <span>.</span>NFT
              </a>
            </h1>
          </div>

          <nav id="navbar" className="navbar">
            <ul>
              <li>
                <a className="nav-link scrollto active" href="#hero">
                  Home
                </a>
              </li>
              <li>
                <a className="nav-link scrollto" href="#news">
                  News
                </a>
              </li>
              <li>
                <a className="nav-link scrollto" href="#services">
                  Collections
                </a>
              </li>
              <li>
                <a className="nav-link scrollto" href="#features">
                  About
                </a>
              </li>
              <li>
              <a className="nav-link scrollto" href="#portfolio">
                Auction
              </a>
            </li>
              <li>
                <a className="getstarted scrollto" href="#about">
                  Gallery
                </a>
              </li>
            </ul>
            <i className="bi bi-list mobile-nav-toggle"></i>
          </nav>
        </div>
      </header>

      <section id="hero">
        <div className="container">
          <div className="row d-flex align-items-center">
            <div
              className="col-lg-6 py-5 py-lg-0 order-2 order-lg-1"
              data-aos="fade-right"
            >
              <h1>Underwater NFT collections</h1>
              <h2>
              Added value to digital assets for underwater photographers<br />and collectors of non-fungible artworks.
              </h2>
              <a href="#news" className="btn-get-started scrollto">
                Gallery
              </a>
            </div>
            <div
              className="col-lg-6 order-1 order-lg-2 hero-img"
              data-aos="fade-left"
            >
              <Image src="/img/hero-img.png" className="img-fluid" alt="" />
            </div>
          </div>
        </div>
      </section>

      <main id="main">
        <section id="clients" className="clients section-bg">
          <div className="container">
            <div className="row no-gutters clients-wrap clearfix wow fadeInUp">
              <div className="col-lg-2 col-md-4 col-6">
                <div className="client-logo">
                  <Image
                    src="/img/clients/client-1.png"
                    className="img-fluid"
                    alt=""
                    data-aos="flip-right"
                  />
                </div>
              </div>

              <div className="col-lg-2 col-md-4 col-6">
                <div className="client-logo">
                  <Image
                    src="/img/clients/client-2.png"
                    className="img-fluid"
                    alt=""
                    data-aos="flip-right"
                    data-aos-delay="100"
                  />
                </div>
              </div>
              <div className="col-lg-2 col-md-4 col-6">
                <div className="client-logo">
                  <Image
                    src="/img/clients/client-3.png"
                    className="img-fluid"
                    alt=""
                    data-aos="flip-right"
                    data-aos-delay="200"
                  />
                </div>
              </div>

              <div className="col-lg-2 col-md-4 col-6">
                <div className="client-logo">
                  <Image
                    src="/img/clients/client-4.png"
                    className="img-fluid"
                    alt=""
                    data-aos="flip-right"
                    data-aos-delay="300"
                  />
                </div>
              </div>

              <div className="col-lg-2 col-md-4 col-6">
                <div className="client-logo">
                  <Image
                    src="/img/clients/client-5.png"
                    className="img-fluid"
                    alt=""
                    data-aos="flip-right"
                    data-aos-delay="400"
                  />
                </div>
              </div>

              <div className="col-lg-2 col-md-4 col-6">
                <div className="client-logo">
                  <Image
                    src="/img/clients/client-6.png"
                    className="img-fluid"
                    alt=""
                    data-aos="flip-right"
                    data-aos-delay="500"
                  />
                </div>
              </div>
            </div>
          </div>
        </section>

        <section id="news" className="about section-bg">
          <div className="container">
            <div className="row gy-4">
              <div className="image col-xl-5"></div>
              <div className="col-xl-7">
                <div className="content d-flex flex-column justify-content-center ps-0 ps-xl-4">
                  <h3 data-aos="fade-in" data-aos-delay="100">
                    Latest News
                  </h3>
                  <p data-aos="fade-in">
                    New arrival of sweet underwater pictures from cebu province.
                    <br />
                    Awesome shots of a sardine school getting hunted by tunas.
                    <br />
                    Picrures taken somwhere near moalboal, panagsama beach.
                    <br />
                    Be the first one to bid and get a free can of sardines.
                  </p>
                  <div className="row gy-4 mt-3">
                    <div className="col-md-6 icon-box" data-aos="fade-up">
                      <i className="bx bx-receipt"></i>
                      <h4>
                        <a href="#">Corporis voluptates sit</a>
                      </h4>
                      <p>
                        Consequuntur sunt aut quasi enim aliquam quae harum
                        pariatur laboris nisi ut aliquip
                      </p>
                    </div>
                    {/* <div
                      className="col-md-6 icon-box"
                      data-aos="fade-up"
                      data-aos-delay="100"
                    >
                      <i className="bx bx-cube-alt"></i>
                      <h4>
                        <a href="#">Ullamco laboris nisi</a>
                      </h4>
                      <p>
                        Excepteur sint occaecat cupidatat non proident, sunt in
                        culpa qui officia deserunt
                      </p>{' '}
                    </div> */}
                    {/* <div
                      className="col-md-6 icon-box"
                      data-aos="fade-up"
                      data-aos-delay="200"
                    >
                      <i className="bx bx-images"></i>
                      <h4>
                        <a href="#">Labore consequatur</a>
                      </h4>
                      <p>
                        Aut suscipit aut cum nemo deleniti aut omnis. Doloribus
                        ut maiores omnis facere
                      </p>
                    </div> */}
                    {/* <div
                      className="col-md-6 icon-box"
                      data-aos="fade-up"
                      data-aos-delay="300"
                    >
                      <i className="bx bx-shield"></i>
                      <h4>
                        <a href="#">Beatae veritatis</a>
                      </h4>
                      <p>
                        Expedita veritatis consequuntur nihil tempore laudantium
                        vitae denat pacta
                      </p>
                    </div> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id="services" className="services section-bg">
          <div className="container">
            <div className="section-title">
              <h2 data-aos="fade-in">Current Collections</h2>
              <p data-aos="fade-in">
                View or newest collections on OpenSea, the world's largest NFT
                marketplace
              </p>
              <p>
                <br />
                <Image src="/img/opensea.png" alt="" />
              </p>
            </div>

            <div className="row">
            <div className="col-md-6 d-flex" data-aos="fade-right">
            <div className="card">
              <div className="card-img">
                <Image src="/img/nemo1.png" alt="" />
              </div>
              <div className="card-body">
                <h5 className="card-title">
                  <a href="">Hurghada</a>
                </h5>
                <p className="card-text">
                  Lorem ipsum dolor sit amet, consectetur elit, sed do
                  eiusmod tempor ut labore et dolore magna aliqua. Ut enim
                  ad minim veniam, quis nostrud exercitation ullamco laboris
                  nisi ut aliquip ex ea commodo consequat
                </p>
                <div className="read-more">
                  <a href="#">
                    <i className="bi bi-arrow-right"></i> collection
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 d-flex" data-aos="fade-left">
            <div className="card">
              <div className="card-img">
                <Image src="/img/nemo2.png" alt="" />
              </div>
              <div className="card-body">
                <h5 className="card-title">
                  <a href="">Bohol</a>
                </h5>
                <p className="card-text">
                  Sed ut perspiciatis unde omnis iste natus error sit
                  voluptatem doloremque laudantium, totam rem aperiam, eaque
                  ipsa quae ab illo inventore veritatis et quasi architecto
                  beatae vitae dicta sunt explicabo
                </p>
                <div className="read-more">
                  <a href="#">
                    <i className="bi bi-arrow-right"></i> collection
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 d-flex" data-aos="fade-right">
                <div className="card">
                  <div className="card-img">
                    <Image src="/img/nemo1.png" alt="" />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">
                      <a href="">Cebu</a>
                    </h5>
                    <p className="card-text">
                      Lorem ipsum dolor sit amet, consectetur elit, sed do
                      eiusmod tempor ut labore et dolore magna aliqua. Ut enim
                      ad minim veniam, quis nostrud exercitation ullamco laboris
                      nisi ut aliquip ex ea commodo consequat
                    </p>
                    <div className="read-more">
                      <a href="#">
                        <i className="bi bi-arrow-right"></i> collection
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6 d-flex" data-aos="fade-left">
                <div className="card">
                  <div className="card-img">
                    <Image src="/img/nemo2.png" alt="" />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">
                      <a href="">Negros</a>
                    </h5>
                    <p className="card-text">
                      Sed ut perspiciatis unde omnis iste natus error sit
                      voluptatem doloremque laudantium, totam rem aperiam, eaque
                      ipsa quae ab illo inventore veritatis et quasi architecto
                      beatae vitae dicta sunt explicabo
                    </p>
                    <div className="read-more">
                      <a href="#">
                        <i className="bi bi-arrow-right"></i> collection
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className="col-md-6 d-flex" data-aos="fade-right">
                <div className="card">
                  <div className="card-img">
                    <Image src="/img/services-3.jpg" alt="..." />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">
                      <a href="">Veritatis natus nisi</a>
                    </h5>
                    <p className="card-text">
                      Nemo enim ipsam voluptatem quia voluptas sit aut odit aut
                      fugit, sed quia magni dolores eos qui ratione voluptatem
                      sequi nesciunt Neque porro quisquam est, qui dolorem ipsum
                      quia dolor sit amet
                    </p>
                    <div className="read-more">
                      <a href="#">
                        <i className="bi bi-arrow-right"></i> Read More
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className="col-md-6 d-flex" data-aos="fade-left">
                <div className="card">
                  <div className="card-img">
                    <Image src="/img/services-4.jpg" alt="..." />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">
                      <a href="">Aliquam veritatis</a>
                    </h5>
                    <p className="card-text">
                      Nostrum eum sed et autem dolorum perspiciatis. Magni porro
                      quisquam laudantium voluptatem. In molestiae earum ab sit
                      esse voluptatem. Eos ipsam cumque ipsum officiis qui nihil
                      aut incidunt aut
                    </p>
                    <div className="read-more">
                      <a href="#">
                        <i className="bi bi-arrow-right"></i> Read More
                      </a>
                    </div>
                  </div>
                </div>
              </div> */}
            </div>
          </div>
        </section>

        <section id="features" className="features section-bg">
          <div className="container">
            <div className="section-title">
              <h2 data-aos="fade-in">What is this about?</h2>
              <p data-aos="fade-in">
                Magnam dolores commodi suscipit. Necessitatibus eius consequatur
                ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
                quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
              </p>
            </div>

            <div className="row content">
              <div className="col-md-5" data-aos="fade-right">
                <Image
                  src="/img/explained/nft.png"
                  className="img-fluid"
                  alt=""
                />
              </div>
              <div className="col-md-7 pt-4" data-aos="fade-left">
                <h3>What are NFTs</h3>
                <p>
                  An NFT is a digital asset that represents real-world objects
                  like in this case photographs.
                  <br />
                  They are bought and sold online, frequently with
                  blockchain-based cryptocurrency.
                  <br />
                  Non-fungible means that it’s unique and can’t be replaced with
                  something else.
                </p>
              </div>
            </div>

            <div className="row content">
              <div className="col-md-5 order-1 order-md-2" data-aos="fade-left">
                <Image
                  src="/img/explained/monkey.png"
                  className="img-fluid"
                  alt=""
                />
              </div>
              <div
                className="col-md-7 pt-5 order-2 order-md-1"
                data-aos="fade-right"
              >
                <h3>Our NFT Themes</h3>
                <p>
                  The NFTs in this gallery are not endless clones of monkey
                  cartoons. <br />
                  Rather the photographs are real snapshots of the beautiful
                  aquatic life in our oceans.
                  <br />
                  Get yourself the ownership of our digital artworks before the
                  last coral reef has gone.
                </p>
              </div>
            </div>

            <div className="row content">
              <div className="col-md-5" data-aos="fade-right">
                <Image
                  src="/img/explained/map.png"
                  className="img-fluid"
                  alt=""
                />
              </div>
              <div className="col-md-7 pt-5" data-aos="fade-left">
                <h3>Photographer Location</h3>
                <p>
                  Pictures of underwater creatures are mostly taken in the
                  tropical tropical water around the philippines. <br />
                  You can find some of the best macro objects in this area of
                  the pacific ocean. <br />
                  During covid travel restrictions some NFTs might been taken at
                  the red sea in egypt as well.
                </p>
              </div>
            </div>

            <div className="row content">
              <div className="col-md-5 order-1 order-md-2" data-aos="fade-left">
                <Image
                  src="/img/explained/olympus.png"
                  className="img-fluid"
                  alt=""
                />
              </div>
              <div
                className="col-md-7 pt-5 order-2 order-md-1"
                data-aos="fade-right"
              >
                <h3>Equipment</h3>
                <p>
                  The pictures are mostly taken with a olympus tg-6 camera.
                  <br />Macro shots of tiny animals are taken with a strobe and sometimes with common torches.
                  <br />Captures of big fish an schools ate taken with a special wide angle lens.
                </p>
              </div>
            </div>

            {/* <div className="row content">
              <div className="col-md-5 order-1 order-md-2" data-aos="fade-left">
                <Image src="/img/features-4.svg" className="img-fluid" alt="" />
              </div>
              <div
                className="col-md-7 pt-5 order-2 order-md-1"
                data-aos="fade-right"
              >
                <h3>
                  Quas et necessitatibus eaque impedit ipsum animi consequatur
                  incidunt in
                </h3>
                <p className="fst-italic">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
                <p>
                  Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                  aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint
                  occaecat cupidatat non proident, sunt in culpa qui officia
                  deserunt mollit anim id est laborum
                </p>
              </div>
            </div> */}
          </div>
        </section>

        <section id="portfolio" className="portfolio section-bg">
          <div className="container">
            <div className="section-title">
              <h2 data-aos="fade-in">Items on Auction</h2>
              <p data-aos="fade-in">
                Magnam dolores commodi suscipit. Necessitatibus eius consequatur
                ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
                quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
                Quia fugiat sit in iste officiis commodi quidem hic quas.
              </p>
            </div>

            <div className="row">
              <div className="col-lg-12">
                <ul id="portfolio-flters">
                  <li data-filter="*" className="filter-active">
                    All
                  </li>
                  <li data-filter=".filter-hrg">HRG</li>
                  <li data-filter=".filter-tag">TAG</li>
                </ul>
              </div>
            </div>

            <div className="row portfolio-container" data-aos="fade-up">
              <div className="col-lg-4 col-md-6 portfolio-item filter-hrg">
                <div className="portfolio-wrap">
                  <Image
                    src="/img/portfolio/portfolio-1.jpg"
                    className="img-fluid"
                    alt=""
                  />
                  <div className="portfolio-links">
                    <a
                      href="/img/portfolio/portfolio-1.jpg"
                      data-gallery="portfolioGallery"
                      className="portfolio-lightbox"
                      title="Scorpionfish"
                    >
                      <i className="bi bi-plus"></i>
                    </a>
                    <a href="portfolio-details.html" title="More Details">
                      <i className="bi bi-link"></i>
                    </a>
                  </div>
                  <div className="portfolio-info">
                    <h4>Scorpionfish</h4>
                    <p>HRG</p>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 portfolio-item filter-hrg">
                <div className="portfolio-wrap">
                  <Image
                    src="/img/portfolio/portfolio-2.jpg"
                    className="img-fluid"
                    alt=""
                  />
                  <div className="portfolio-links">
                    <a
                      href="/img/portfolio/portfolio-2.jpg"
                      data-gallery="portfolioGallery"
                      className="portfolio-lightbox"
                      title="Moray eel"
                    >
                      <i className="bi bi-plus"></i>
                    </a>
                    <a href="portfolio-details.html" title="More Details">
                      <i className="bi bi-link"></i>
                    </a>
                  </div>
                  <div className="portfolio-info">
                    <h4>Moray eel</h4>
                    <p>HRG</p>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 portfolio-item filter-hrg">
                <div className="portfolio-wrap">
                  <Image
                    src="/img/portfolio/portfolio-3.jpg"
                    className="img-fluid"
                    alt=""
                  />
                  <div className="portfolio-links">
                    <a
                      href="/img/portfolio/portfolio-3.jpg"
                      data-gallery="portfolioGallery"
                      className="portfolio-lightbox"
                      title="Lionsfish"
                    >
                      <i className="bi bi-plus"></i>
                    </a>
                    <a href="portfolio-details.html" title="More Details">
                      <i className="bi bi-link"></i>
                    </a>
                  </div>
                  <div className="portfolio-info">
                    <h4>Lionsfish</h4>
                    <p>HRG</p>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 portfolio-item filter-tag">
                <div className="portfolio-wrap">
                  <Image
                    src="/img/portfolio/portfolio-1.jpg"
                    className="img-fluid"
                    alt=""
                  />
                  <div className="portfolio-links">
                    <a
                      href="/img/portfolio/portfolio-1.jpg"
                      data-gallery="portfolioGallery"
                      className="portfolio-lightbox"
                      title="Scorpionfish"
                    >
                      <i className="bi bi-plus"></i>
                    </a>
                    <a href="portfolio-details.html" title="More Details">
                      <i className="bi bi-link"></i>
                    </a>
                  </div>
                  <div className="portfolio-info">
                    <h4>Scorpionfish</h4>
                    <p>HRG</p>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 portfolio-item filter-tag">
                <div className="portfolio-wrap">
                  <Image
                    src="/img/portfolio/portfolio-2.jpg"
                    className="img-fluid"
                    alt=""
                  />
                  <div className="portfolio-links">
                    <a
                      href="/img/portfolio/portfolio-2.jpg"
                      data-gallery="portfolioGallery"
                      className="portfolio-lightbox"
                      title="Moray eel"
                    >
                      <i className="bi bi-plus"></i>
                    </a>
                    <a href="portfolio-details.html" title="More Details">
                      <i className="bi bi-link"></i>
                    </a>
                  </div>
                  <div className="portfolio-info">
                    <h4>Moray eel</h4>
                    <p>HRG</p>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 portfolio-item filter-tag">
                <div className="portfolio-wrap">
                  <Image
                    src="/img/portfolio/portfolio-3.jpg"
                    className="img-fluid"
                    alt=""
                  />
                  <div className="portfolio-links">
                    <a
                      href="/img/portfolio/portfolio-3.jpg"
                      data-gallery="portfolioGallery"
                      className="portfolio-lightbox"
                      title="Lionsfish"
                    >
                      <i className="bi bi-plus"></i>
                    </a>
                    <a href="portfolio-details.html" title="More Details">
                      <i className="bi bi-link"></i>
                    </a>
                  </div>
                  <div className="portfolio-info">
                    <h4>Lionsfish</h4>
                    <p>HRG</p>
                  </div>
                </div>
              </div>

              {/* <div className="col-lg-4 col-md-6 portfolio-item filter-card">
                <div className="portfolio-wrap">
                  <Image
                    src="/img/portfolio/portfolio-4.jpg"
                    className="img-fluid"
                    alt=""
                  />
                  <div className="portfolio-links">
                    <a
                      href="/img/portfolio/portfolio-4.jpg"
                      data-gallery="portfolioGallery"
                      className="portfolio-lightbox"
                      title="Card 2"
                    >
                      <i className="bi bi-plus"></i>
                    </a>
                    <a href="portfolio-details.html" title="More Details">
                      <i className="bi bi-link"></i>
                    </a>
                  </div>
                  <div className="portfolio-info">
                    <h4>Card 2</h4>
                    <p>Card</p>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 portfolio-item filter-web">
                <div className="portfolio-wrap">
                  <Image
                    src="/img/portfolio/portfolio-5.jpg"
                    className="img-fluid"
                    alt=""
                  />
                  <div className="portfolio-links">
                    <a
                      href="/img/portfolio/portfolio-5.jpg"
                      data-gallery="portfolioGallery"
                      className="portfolio-lightbox"
                      title="Web 2"
                    >
                      <i className="bi bi-plus"></i>
                    </a>
                    <a href="portfolio-details.html" title="More Details">
                      <i className="bi bi-link"></i>
                    </a>
                  </div>
                  <div className="portfolio-info">
                    <h4>Web 2</h4>
                    <p>Web</p>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 portfolio-item filter-app">
                <div className="portfolio-wrap">
                  <Image
                    src="/img/portfolio/portfolio-6.jpg"
                    className="img-fluid"
                    alt=""
                  />
                  <div className="portfolio-links">
                    <a
                      href="/img/portfolio/portfolio-6.jpg"
                      data-gallery="portfolioGallery"
                      className="portfolio-lightbox"
                      title="App 3"
                    >
                      <i className="bi bi-plus"></i>
                    </a>
                    <a href="portfolio-details.html" title="More Details">
                      <i className="bi bi-link"></i>
                    </a>
                  </div>
                  <div className="portfolio-info">
                    <h4>App 3</h4>
                    <p>App</p>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 portfolio-item filter-card">
                <div className="portfolio-wrap">
                  <Image
                    src="/img/portfolio/portfolio-7.jpg"
                    className="img-fluid"
                    alt=""
                  />
                  <div className="portfolio-links">
                    <a
                      href="/img/portfolio/portfolio-7.jpg"
                      data-gallery="portfolioGallery"
                      className="portfolio-lightbox"
                      title="Card 1"
                    >
                      <i className="bi bi-plus"></i>
                    </a>
                    <a href="portfolio-details.html" title="More Details">
                      <i className="bi bi-link"></i>
                    </a>
                  </div>
                  <div className="portfolio-info">
                    <h4>Card 1</h4>
                    <p>Card</p>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 portfolio-item filter-card">
                <div className="portfolio-wrap">
                  <Image
                    src="/img/portfolio/portfolio-8.jpg"
                    className="img-fluid"
                    alt=""
                  />
                  <div className="portfolio-links">
                    <a
                      href="/img/portfolio/portfolio-8.jpg"
                      data-gallery="portfolioGallery"
                      className="portfolio-lightbox"
                      title="Card 3"
                    >
                      <i className="bi bi-plus"></i>
                    </a>
                    <a href="portfolio-details.html" title="More Details">
                      <i className="bi bi-link"></i>
                    </a>
                  </div>
                  <div className="portfolio-info">
                    <h4>Card 3</h4>
                    <p>Card</p>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 portfolio-item filter-web">
                <div className="portfolio-wrap">
                  <Image
                    src="/img/portfolio/portfolio-9.jpg"
                    className="img-fluid"
                    alt=""
                  />
                  <div className="portfolio-links">
                    <a
                      href="/img/portfolio/portfolio-9.jpg"
                      data-gallery="portfolioGallery"
                      className="portfolio-lightbox"
                      title="Web 3"
                    >
                      <i className="bi bi-plus"></i>
                    </a>
                    <a href="portfolio-details.html" title="More Details">
                      <i className="bi bi-link"></i>
                    </a>
                  </div>
                  <div className="portfolio-info">
                    <h4>Web 3</h4>
                    <p>Web</p>
                  </div>
                </div>
              </div> */}
            </div>

            <div className="row content">
              <div className="col-md-5">
                  <Image src="/img/air.png" className="img-fluid" alt=""/>
              </div>
              <div className="col-md-7 pt-5">
                <p>
                0,002 ETH gives us air for estimated 15 minutes under water.
                <br />
                Help us to dive longer and take more pictures of this amazing creatures.
                </p>
              </div>
            </div>

          </div>
        </section>

        {/* <Team /> */}

        {/* <section id="pricing" className="pricing section-bg">
        <div className="container">
          <div className="section-title">
            <h2 data-aos="fade-in">Pricing</h2>
            <p data-aos="fade-in">
              Magnam dolores commodi suscipit. Necessitatibus eius consequatur
              ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
              quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
              Quia fugiat sit in iste officiis commodi quidem hic quas.
            </p>
          </div>

          <div className="row no-gutters">
            <div className="col-lg-4 box" data-aos="zoom-in">
              <h3>Free</h3>
              <h4>
                $0<span>per month</span>
              </h4>
              <ul>
                <li>
                  <i className="bx bx-check"></i> Quam adipiscing vitae proin
                </li>
                <li>
                  <i className="bx bx-check"></i> Nec feugiat nisl pretium
                </li>
                <li>
                  <i className="bx bx-check"></i> Nulla at volutpat diam uteera
                </li>
                <li className="na">
                  <i className="bx bx-x"></i>{' '}
                  <span>Pharetra massa massa ultricies</span>
                </li>
                <li className="na">
                  <i className="bx bx-x"></i>{' '}
                  <span>Massa ultricies mi quis hendrerit</span>
                </li>
              </ul>
              <a href="#" className="get-started-btn">
                Get Started
              </a>
            </div>

            <div className="col-lg-4 box featured" data-aos="zoom-in">
              <span className="featured-badge">Featured</span>
              <h3>Business</h3>
              <h4>
                $29<span>per month</span>
              </h4>
              <ul>
                <li>
                  <i className="bx bx-check"></i> Quam adipiscing vitae proin
                </li>
                <li>
                  <i className="bx bx-check"></i> Nec feugiat nisl pretium
                </li>
                <li>
                  <i className="bx bx-check"></i> Nulla at volutpat diam uteera
                </li>
                <li>
                  <i className="bx bx-check"></i> Pharetra massa massa ultricies
                </li>
                <li>
                  <i className="bx bx-check"></i> Massa ultricies mi quis
                  hendrerit
                </li>
              </ul>
              <a href="#" className="get-started-btn">
                Get Started
              </a>
            </div>

            <div className="col-lg-4 box" data-aos="zoom-in">
              <h3>Developer</h3>
              <h4>
                $49<span>per month</span>
              </h4>
              <ul>
                <li>
                  <i className="bx bx-check"></i> Quam adipiscing vitae proin
                </li>
                <li>
                  <i className="bx bx-check"></i> Nec feugiat nisl pretium
                </li>
                <li>
                  <i className="bx bx-check"></i> Nulla at volutpat diam uteera
                </li>
                <li>
                  <i className="bx bx-check"></i> Pharetra massa massa ultricies
                </li>
                <li>
                  <i className="bx bx-check"></i> Massa ultricies mi quis
                  hendrerit
                </li>
              </ul>
              <a href="#" className="get-started-btn">
                Get Started
              </a>
            </div>
          </div>
        </div>
      </section> */}

        {/* <section id="faq" className="faq section-bg">
        <div className="container">
          <div className="section-title">
            <h2 data-aos="fade-in">Frequently Asked Questions</h2>
            <p data-aos="fade-in">
              Magnam dolores commodi suscipit. Necessitatibus eius consequatur
              ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
              quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
              Quia fugiat sit in iste officiis commodi quidem hic quas.
            </p>
          </div>

          <div
            className="row faq-item d-flex align-items-stretch"
            data-aos="fade-up"
          >
            <div className="col-lg-5">
              <i className="bx bx-help-circle"></i>
              <h4>Non consectetur a erat nam at lectus urna duis?</h4>
            </div>
            <div className="col-lg-7">
              <p>
                Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id
                volutpat lacus laoreet non curabitur gravida. Venenatis lectus
                magna fringilla urna porttitor rhoncus dolor purus non.
              </p>
            </div>
          </div>

          <div
            className="row faq-item d-flex align-items-stretch"
            data-aos="fade-up"
            data-aos-delay="100"
          >
            <div className="col-lg-5">
              <i className="bx bx-help-circle"></i>
              <h4>
                Feugiat scelerisque varius morbi enim nunc faucibus a
                pellentesque?
              </h4>
            </div>
            <div className="col-lg-7">
              <p>
                Dolor sit amet consectetur adipiscing elit pellentesque habitant
                morbi. Id interdum velit laoreet id donec ultrices. Fringilla
                phasellus faucibus scelerisque eleifend donec pretium. Est
                pellentesque elit ullamcorper dignissim.
              </p>
            </div>
          </div>

          <div
            className="row faq-item d-flex align-items-stretch"
            data-aos="fade-up"
            data-aos-delay="200"
          >
            <div className="col-lg-5">
              <i className="bx bx-help-circle"></i>
              <h4>
                Dolor sit amet consectetur adipiscing elit pellentesque habitant
                morbi?
              </h4>
            </div>
            <div className="col-lg-7">
              <p>
                Eleifend mi in nulla posuere sollicitudin aliquam ultrices
                sagittis orci. Faucibus pulvinar elementum integer enim. Sem
                nulla pharetra diam sit amet nisl suscipit. Rutrum tellus
                pellentesque eu tincidunt. Lectus urna duis convallis convallis
                tellus.
              </p>
            </div>
          </div>

          <div
            className="row faq-item d-flex align-items-stretch"
            data-aos="fade-up"
            data-aos-delay="300"
          >
            <div className="col-lg-5">
              <i className="bx bx-help-circle"></i>
              <h4>
                Ac odio tempor orci dapibus. Aliquam eleifend mi in nulla?
              </h4>
            </div>
            <div className="col-lg-7">
              <p>
                Aperiam itaque sit optio et deleniti eos nihil quidem cumque.
                Voluptas dolorum accusantium sunt sit enim. Provident
                consequuntur quam aut reiciendis qui rerum dolorem sit odio.
                Repellat assumenda soluta sunt pariatur error doloribus fuga.
              </p>
            </div>
          </div>

          <div
            className="row faq-item d-flex align-items-stretch"
            data-aos="fade-up"
            data-aos-delay="400"
          >
            <div className="col-lg-5">
              <i className="bx bx-help-circle"></i>
              <h4>
                Tempus quam pellentesque nec nam aliquam sem et tortor
                consequat?
              </h4>
            </div>
            <div className="col-lg-7">
              <p>
                Molestie a iaculis at erat pellentesque adipiscing commodo.
                Dignissim suspendisse in est ante in. Nunc vel risus commodo
                viverra maecenas accumsan. Sit amet nisl suscipit adipiscing
                bibendum est. Purus gravida quis blandit turpis cursus in
              </p>
            </div>
          </div>
        </div>
      </section> */}

        {/* <section id="contact" className="contact section-bg">
        <div className="container" data-aos="fade-up">
          <div className="section-title">
            <h2>Contact</h2>
            <p>
              Magnam dolores commodi suscipit. Necessitatibus eius consequatur
              ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
              quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
              Quia fugiat sit in iste officiis commodi quidem hic quas.
            </p>
          </div>

          <div className="row">
            <div className="col-lg-6">
              <div className="row">
                <div className="col-md-12">
                  <div className="info-box" data-aos="fade-up">
                    <i className="bx bx-map"></i>
                    <h3>Our Address</h3>
                    <p>A108 Adam Street, New York, NY 535022</p>
                  </div>
                </div>
                <div className="col-md-6">
                  <div
                    className="info-box mt-4"
                    data-aos="fade-up"
                    data-aos-delay="100"
                  >
                    <i className="bx bx-envelope"></i>
                    <h3>Email Us</h3>
                    <p>
                      info@example.com
                      <br />
                      contact@example.com
                    </p>
                  </div>
                </div>
                <div className="col-md-6">
                  <div
                    className="info-box mt-4"
                    data-aos="fade-up"
                    data-aos-delay="100"
                  >
                    <i className="bx bx-phone-call"></i>
                    <h3>Call Us</h3>
                    <p>
                      +1 5589 55488 55
                      <br />
                      +1 6678 254445 41
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-lg-6 mt-4 mt-lg-0">
              <form
                action="forms/contact.php"
                method="post"
                role="form"
                className="php-email-form w-100"
                data-aos="fade-up"
              >
                <div className="row">
                  <div className="col-md-6 form-group">
                    <input
                      type="text"
                      name="name"
                      className="form-control"
                      id="name"
                      placeholder="Your Name"
                      required
                    />
                  </div>
                  <div className="col-md-6 form-group mt-3 mt-md-0">
                    <input
                      type="email"
                      className="form-control"
                      name="email"
                      id="email"
                      placeholder="Your Email"
                      required
                    />
                  </div>
                </div>
                <div className="form-group mt-3">
                  <input
                    type="text"
                    className="form-control"
                    name="subject"
                    id="subject"
                    placeholder="Subject"
                    required
                  />
                </div>
                <div className="form-group mt-3">
                  <textarea
                    className="form-control"
                    name="message"
                    rows="5"
                    placeholder="Message"
                    required
                  ></textarea>
                </div>
                <div className="my-3">
                  <div className="loading">Loading</div>
                  <div className="error-message"></div>
                  <div className="sent-message">
                    Your message has been sent. Thank you!
                  </div>
                </div>
                <div className="text-center">
                  <button type="submit">Send Message</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section> */}
      </main>

      <footer id="footer">
        <div className="footer-top">
          <div className="container">
            <div className="row  justify-content-center">
              <div className="col-lg-6">
                <h3>underwater.NFT</h3>
                <p>
                The planet has lost half of its coral reefs since 1950
                <br/>This most diverse ecosystem on earth also provides food to millions of humans
                <br/>Pleas don't try to eat the pothographs - they taste awefull</p>
                <p><Image src="/img/olympus.png" alt="" /></p>
              </div>
            </div>

            {/* 
            <div className="row footer-newsletter justify-content-center">
              <div className="col-lg-6">
                <form action="" method="post">
                  <input
                    type="email"
                    name="email"
                    placeholder="Enter your Email"
                  />
                  <input type="submit" value="Contact" />
                </form>
              </div>
            </div>
            */}

            <div className="social-links">
              <a href="#" className="xxx">
                <BxlFacebookCircleIcon />
              </a>
              <a href="#" className="facebook">
                <BxlFacebookCircleIcon />
              </a>
              <a href="#" className="facebook">
                <BxlFacebookCircleIcon />
              </a>
            </div>
          </div>
        </div>

        <div className="container footer-bottom clearfix">
          <div className="copyright">
            &copy; Copyright {year()}{' '}
            <strong>
              <span>underwater.NFT</span>
            </strong>
            . All Rights Reserved
          </div>
          <div className="credits">
            Created with <a href="https://reactjs.org/">React</a>,{' '}
            <a href="https://nextjs.org/">Next.JS</a> and{' '}
            <a href="https://strapi.io/">Strapi</a>
          </div>
        </div>
      </footer>

      <a
        href="#"
        className="back-to-top d-flex align-items-center justify-content-center"
      >
        <i className="bi bi-arrow-up-short"></i>
      </a>

      {/* <script src="/vendor/aos/aos.js"></script>
  <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="/vendor/php-email-form/validate.js"></script> */}
    </>
  );
}

export default Index;
